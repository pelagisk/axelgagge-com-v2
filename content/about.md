---
title: "About"
date: 2022-11-20T09:03:20-08:00
---

I am based in Stockholm and work with art and science.

I am partial to collaboration – the rare and precious kind where you give more than is expected of you, where utility and status is replaced by opaque and complex culture-making, where you help each other develop beyond fixed identities. I try to nurture such collectivities wherever I find them.

I spend a decade studying and working with theoretical physics, until I attained a PhD degree from Stockholm University in 2023. I have focused my attention on quantum matter, gases/liquids/solids where quantum physics interfere in the material properties. Complex systems of strong non-linearities and feedback are particularly interesting to me, since they are close to the horizon of the computable and knowable. My craft is to create and critique computer simulations of such systems, gradually building intuition for the relation of equation and physics. 

The discrimination and exclusion which still dominates science of course also excludes many ideas: contemporary physics does not think in multiplicity like, say, biology or archeology. Still, I have discovered many "leakages" through which physics could become something else, e.g.: the visualizations used to relate equations to the sensible, the role of infinities in perturbation theory or the problem of quantum physics itself. 

Since 2019, I have been learning and practicing artistic crafts instead. In textile, printmaking, woodworking, essay writing and installation, I find the means to express something more than just plainly stating or computing a result. Still, I think my work is based on a kind of second-order realism and pedagogy.

By unlearning or rather tweaking/hacking my scientific training I am trying to find a way to combine the knowledges of the sensible, of the hands and body, with the knowledge of mathematics and computers. I believe the divide between scientific and artistic knowledges is a key political problem (think social organization and climate). We cannot survive without merging them, even if it is infinitely hard.

Right now, I am most focused on collective artistic process with [Cluster 8](https://www.cluster8.org/), my main collaborators. I am also writing and developing my skills in programming and machine learning


## Education

- PhD in theoretical and computational physics (2023)
- BFA from the Royal Academy of Art in Stockholm (2023)


### Contact

Email: axel.gagge [at] gmail.com

Instagram: [@axelgagge](https://www.instagram.com/axelgagge/)


### Links

My PhD thesis can be accessed [here](https://urn.kb.se/resolve?urn=urn:nbn:se:su:diva-215403)

All my other academic publications can be accessed [here](http://www.diva-portal.org/smash/resultList.jsf?query=Axel+Gagge&language=sv&searchType=SIMPLE&noOfRows=50&sortOrder=author_sort_asc&sortOrder2=title_sort_asc&onlyFullText=false&sf=all&aq=%5B%5B%5D%5D&aqe=%5B%5D&aq2=%5B%5B%5D%5D&af=%5B%5D).