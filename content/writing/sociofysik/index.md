---
title: Sociofysik
published: "2025-01-07"
---

*Ursprungligen publicerad i OEI -- Organisering 2024*

Sociofysik eller social fysik är namnet på en strävan att tillämpa fysikens metoder och världsbild på organiseringen av mänskliga samhällen. Kvantitativa metoder och matematiska modeller har redan ett stort inflytande i humaniora, konst, filosofi och politik, men sociofysiken går ett steg längre genom att identifiera sina matematiska modeller med det samhälle de försöker beskriva. 

I den här artikeln vill jag ge en introduktion till sociofysikens ambitioner och politiska konsekvenser samt sätta den i ett filosofiskt och konstnärligt sammanhang. På vägen tar jag upp flera exempel och kommer in på ett antal sidospår. Jag utgår till stor del från naturvetenskapens perspektiv: det är ett medvetet val för att visa att det går att komma fram till en kritisk förståelse av sociofysiken även från denna synvinkel. Givetvis finns många andra kompletterande perspektiv på ämnet, till exempel mediateoretiskt eller vetenskapsfilosofiskt.  

Artikeln tar formen av en essä i tre delar. Den första delen är en granskning av (socio-)fysikens världsbild och utgångspunkter, vilka görs synliga genom konkreta exempel. Den andra delen är en undersökning av hur fysiken föreställer sig att form uppstår och därmed också hur sociofysiken tänker sig att mänskliga organisationer bildas. Den tredje delen är en kritisk granskning och en skiss över tänkbara strategier för motstånd i en värld där kapitalistisk och matematisk kontroll flyter samman. 

# 1: Sociofysikens världsbild

## System och modeller

Fysiker tenderar att göra sina egna utgångspunkter osynliga genom att inte göra åtskillnad på matematiska tankekonstruktioner och verkligheten. Det påverkar hur matematik och fysik förstås och diskuteras vardagligt i samhället. För att förstå hur denna sammanblandning uppkommit, ska vi börja med att undersöka fysikens utgångspunkter.

Fysiken studerar system som antas vara isolerade från sin omgivning. Den utvecklar matematiska modeller (hädanefter endast kallade modeller) vars syfte är att vara avbildningar av systemet. En modell anses bra om den är användbar för att förutsäga experiment. Samtidigt är en modell bara användbar om den samtidigt är tillräckligt enkel och avskalad: endast då erbjuder den en förklaring, ett “varför”. Hur modellerna och verkligheten ska jämföras är på inget sätt självklart utan är föremål för otaliga teoretiska diskussioner såväl som konkreta oklarheter [1]. 

En modell består av två delar: dels tillståndet som innehåller den information som krävs för att beskriva modellen vid en viss tidpunkt.  Dels rörelseekvationerna som ibland kallas för lagar: dessa är differens- eller differentialekvationer som beskriver hur ett tillstånd förändras med tiden. Newtons andra lag är det typiska exemplet på en rörelseekvation.

![Modell](modell.png)

*Ett enkelt exempel av en modell av en folkmassa som endast består av ett antal agenter som undviker varandra. Tillståndet T1  utvecklas med tiden till tillståndet T2 enligt rörelseekvationerna (RE). Ekvationerna beskriver hur agenterna rör sig.*

I sociofysiken studeras exempelvis folkmassan som ett system. Tillståndet är i detta fall en uppsättning agenter, modell-individer som besitter en position och en hastighet. Rörelseekvationerna beskriver hur agenter undviker varandra och strävar efter att sprida ut sig (i vissa fall har hänsyn tagits till att agenter attraherar sina familjemedlemmar eller vänner). Även väggar och andra hinder utövar en repellerande kraft. En sådan modell har använts framgångsrikt för att förhindra rusningar i folkmassor där människor kläms till döds [2]. Forskningen pekar på att panik i sig inte är orsaken till att människor kläms till döds i folkmassor. När trängseln blir tillräckligt stor börjar människor röra sig stötvis framåt, vilket i sin tur förstärker trängseln och skapar allt starkare och farligare kollektiva vågor av täthet. Först efteråt uppstår paniken, när trycket i de tätaste delarna av dessa vågor blir tillräcklig för att döda människor. Forskarna visade också att mobiltelefoner kunde användas för att övervaka och larma individer, vilket förhindrade de kollektiva vågorna från att bildas [3]. I sociofysiska modeller av folkmassor utgör individerna en sorts odelbara sociala atomer. Modellens indata består bara av grundläggande utgångspunkter och antaganden om hur dessa sociala atomer borde bete sig i folkmassor, men den ger ändå upphov till nya förklaringar, nya “varför”.

## Simulation

Det är en sak att skapa en modell, en helt annan sak att faktiskt räkna på den. Det är endast från den beräknade tidsutvecklingen som vi kan göra förutsägelser. Utan förutsägelser blir det i sin tur svårt att utvärdera om modellen speglar systemet. Det finns fundamentala gränser för vad som kan beräknas och lösas [4], men datorer har gjort det möjligt att beräkna en stor uppsättning modeller som är omöjliga att lösa för en människa med papper och penna. Ett vanligt tillvägagångssätt är att simulera modellen. Det betyder att “starta” systemet utifrån information om hur tillståndet ser ut just vid en viss tidpunkt. Sedan räknar datorn ut hur det utvecklas i framtiden, steg för steg, med hjälp av rörelseekvationerna. Metoden används både för att förutsäga vädret såväl som för att skapa realistiska datorspel. Datorer skulle alltså kunna sägas utgöra en sorts provrör eller drivhus för modeller. Vi interagerar med simulationerna på en flytande skala mellan experiment, spel och lek. Precis som när vi spelar ett spel så kan fysikers arbete med simulationer ge dem en sinnlig upplevelse av modeller och på så sätt ge upphov till en “virtuell verklighet”.

## Statistiska modeller och maskininlärning

Statistiken och dess senaste inkarnation – maskininlärning (ML, även känd som artificiell intelligens) – är också baserad på modeller, vilka inte förutsäger systemet direkt utan snarare indirekt genom sannolikheter. Ett enkelt exempel är den linjära regressionen, vars modell förutspår att medelvärdet av ett antal datapunkter, var och en av formen (x, y), följer ett linjärt samband: y = kx + m. Sannolikheterna gör relationen mellan modell och system ännu mer komplicerad. Hur sannolikheter och statistik ska förstås har varit en filosofisk fråga de senaste hundra åren [5]. I sociofysiken, den statistiska fysiken och kvantfysiken kombineras i regel slumpvariabler och deterministiska variabler. 

De statistiska modellernas parametrar (egenskaper), vare sig det rör sig om medelvärde, varians eller de många viktningarna inom ML, är inte på förhand givna utan måste bestämmas utifrån situationen. En del av dessa bestäms genom att matematiskt anpassa modellen till mätningar av systemet, vilket i ML kallas att “träna” eller “lära”. Men det finns alltid vissa antaganden i modellen som inte kan bestämmas automatiskt, som inte är möjliga att instrumentellt anpassa, utan kräver mänsklig expertis och intuition. Det rör sig till exempel om urvalet av träningsdata för modellen eller antalet etapper i dess träning. Sådana parametrar kallas även för hyperparametrar [6] [7]. Namnet beskriver på ett slående sätt överskottet av parametrar, överskottet i modellens förmåga att anpassa sig, de överordnade antaganden som måste göras för att anpassa den till konkreta tillämpningar.

## Grafer 

![Exempel på grafer](graph0.png)

*Visuella representationer av fyra olika grafer/nätverk. Noderna representeras som punkter. Bågarna representeras som raka linjer.*

Grafer är matematiska modeller som används för att förstå mänskliga relationer och används både av forskare, företag och statliga aktörer. I vardagligt tal kallas de ofta nätverk, och det ordet har gradvis tappat sina tidigare konnotationer och kommit att bli synonymt med det matematiska begreppet. Precis som annars när det gäller sociofysik har nätverket en normativ roll och nätverket används ofta som en synonym för de relationer som den utger sig för att beskriva. I nätverksmodeller representeras individerna av noder, sammankopplade genom bågar som representerar relationer: sociala kontakter, familjer, arbetsplatser eller internetkontakter. Graferna visualiseras som diagram, där noderna motsvaras av punkter i diagrammet och bågarna motsvaras av streck som förbinder dessa punkter. Graferna har sin egen estetik som föreskriver hur bilderna ska “läsas”, snarare än betraktas: vi tvingas att förbise bågarnas längd, rörelse och böjlighet såväl som punkternas storlek för att effektivt läsa in figurerna.

Nätverket är en sorts universell karta av många olika sorters relationer: i exempelvis sociologi måste denna karta byggas upp för hand, medan digital kommunikation genom sin själva uppbyggnad genererar sådana kartor. Genom att granska “kartan” går det sedan att lära sig mycket om “terrängen”. Kriminella grupper, terrorister och politiska “extremister” granskas av säkerhetstjänster på detta vis. Nätverkens styrkor och sårbarheter kan upptäckas och tillåta både en direkt påverkan (gripanden) och en mer subtil påverkan genom att övervaka eller dämpa nätverkens tillväxt i hemlighet [8]. Samhället som helhet analyseras med nätverksteori för att påverka valresultat och för att skapa misstro eller tilltro. Genom att studera informationsspridning i nätverk tror sig exempelvis forskare ha kunnat förklara hur vågor av sociala rörelser kommer och går – genom den begränsade hastigheten med vilken information kan sprida sig i sådana typer av nätverk [9].

![alt text](graph.png)

*Till vänster: ett slumpmässigt sammanbundet nätverk. Till höger: ett nätverk av skalfri typ.*

Många verkliga nätverk (flygrutter, akademiska samarbeten, politiska nätverk) har en likartad uppbyggnad: majoriteten har en eller ett fåtal förbindelser, men det finns även “hubbar”: noder sammankopplade med många bågar. Men det rör sig inte om en central nod som hierarkiskt är sammankopplad med alla andra, utan om en mångfald av mindre och större hubbar. Sådana nätverk kallas för “skalfria” på grund av den fraktala förekomsten av större och mindre hubbar (se bild). Mångfalden av hubbar gör att information kan spridas särskilt effektivt i sådana nätverk samtidigt som den ger stabilitet: om en hubb skärs av finns oftast många alternativa förbindelser genom nätverket [10]. Det har dock utvecklats metoder för att destabilisera även skalfria nätverk.

Rodrigo Nunes [11] har skrivit om hur forskning om nätverk kan tillämpas i politisk organisering. Han menar att vi måste sluta romantisera både decentralisering och hierarki och istället uppmuntra en mångfald av knutpunkter och se hur den egna organisationen bildar relationer till andra aktörer. Även politiska rörelser och de flesta organisationer kan beskrivas som skalfria nätverk. De är inte centraliserade eller decentraliserade, utan både och samtidigt! De allra flesta är “hangarounds” som känner ett fåtal. Det finns ett fåtal “organisatörer” som gör en stor del av organisationsarbetet och har många kontakter. Typiskt sett finns det en variation av både små och stora organisatörer. På samma vis finns det ofta en variation av ett fåtal stora och ett flertal små tidskrifter, föreningar och möteslokaler. Denna struktur uppstår spontant, men leder samtidigt till ett stabilt nätverk som sprider information effektivt. Det är mindre typiskt att stöta på ett sammanhang där alla känner lika många personer och spelar samma roller. Lika ovanligt är det med nätverk där en liten grupp kontrollerar allt. Forskning om nätverk antyder också att båda dessa typer av organisationer är sköra och dåliga på att sprida information. 

# 2: Sociofysikens organisering

## Återkoppling

Alla system som studeras i sociofysiken är exempel på komplexa system [12]. En typisk egenskap för komplexa system är förekomsten av stark återkoppling eller feedback. Ordet kommer från kretsdiagram i elektronik, där flödena bildar en sluten slinga (de löper tillbaka till sig själva, “feeds back”). Samma fenomen uppstår mellan mikrofoner och högtalare, och kallas då för rundgång. Återkoppling kan vara korrigerande eller förstärkande: korrigerande återkoppling leder till att ett system gradvis korrigerar sitt beteende, anpassar sig i någon mening tills en balans, ett stationärt tillstånd uppstår. 

I modeller av demokratiska val antas att medborgarna endast kan rösta på två partier. Varje sådan agent/medborgare antas vara placerad på en ruta i ett rutnät. Agenterna antas bete sig likt agenterna i sin omedelbara omgivning: i det enklaste fallet antar vi att modell-medborgaren lägger sin röst baserat på medelvärdet av vad grannarna på rutnätet lägger sina röster på. Dessutom antas en viss slumpmässig variation: denna är tänkt att fånga allt som modellen inte tas med, som en sorts bakgrundsbrus. I simulationer där agenterna från början förses med fullständigt slumpmässiga åsikter bildas gradvis sammanhängande områden där ett av de två partierna dominerar. Agenternas strävan att bete sig likt sina grannar utgör en korrigerande återkoppling som stabiliserar områdena. Nästan exakt samma modell används även för att beskriva smittspridning, segregation i bostadsområden och magnetiska material [13]. Det faktum att samma modell kan beskriva flera olika situationer kallas i fysiken för universalitet (termen saknar koppling till politisk universalitet). Från sociofysikens universella perspektiv är smittspridning, magnetiska atomer och politisk förändring verkligen variationer på samma tema [13a]. Förutsättningen för denna universalitet är dock modellens antaganden om homofili: strävan efter likhet och närhet. Utifrån sådana mycket farliga antaganden “återupptäcks” idag redan hat, diskriminering och rasism som neutrala fakta eller till och med som olyckliga men nödvändiga baksidor av en kärlek till den egna gruppen [14]. 

Förstärkande återkoppling leder istället till en explosiv förändring. Ett exempel är sociofysikens modell av trafikflöden: en ojämn inbromsning och acceleration i kombination med hög täthet av bilar leder i dessa modeller till en förstärkande återkoppling: ryckigheten leder till en tätare koncentration av bilar, vilket i sin tur förstärker ryckigheten. Resultatet blir ett totalt stopp i trafiken, en stockning. Ett annat exempel är tröskelvärdena i jordens klimat: om värmen ökar så mycket att permafrosten försvinner, kommer marken att släppa ut växthusgaser som i sin tur skyndar på processen på ett icke-linjärt sätt.

## Självorganisering

> “Då det alltså är ett faktum att ingen vila har skänkts atomerna i det djupa tomrummet och att de snarare drivs i en ständig rörelse åt olika håll studsar somliga av dem långt ifrån varandra efter sin sammanstötning, andra drivs tillbaka bara ett kort stycke av stöten. Och de som drivs samman i en tätare förening och studsar från varandra endast ett obetydligt stycke flätas ihop i sina egna slingrande former och bildar fast rotade klippblock och metallers mäktiga massa och övriga liknande ting [...] Jag kommer att tänka på en bild som är ständigt för våra ögon och som visar att detta är sant: se när solstrålarna tar sig in i ett mörkt rum och sprider ljus där! Du ska i solstrålarnas sken se mängder av små kroppar blandas samman på många sätt i tomrummet och liksom i ett evigt fältslag leverera drabbning på drabbning [...] I någon mån kan ett obetydligt ting ge en föreställning om och en låt vara ofullständig bild av stora ting.”

*Ur “Om Tingens Natur” av Lucretius i Bertil Cavallins översättning*

Vi lever knappast i en värld som är sluten: solens strålar värmer jorden och skapar förutsättningarna för liv. Jorden strålar ut värme genom atmosfären. Därför är det märkligt att fysiken fram till 1900-talets slut fokuserade på isolerade system, avskilda så att varken energi eller materia tillförs eller lämnar dem. Ilya Prigogine var en av de som riktade naturvetenskapens uppmärksamhet mot dissipativa system, genom vilka energi flödar. Han upptäckte bland annat att kemiska föreningar kunde bilda invecklade former och mönster som omöjligt kan bildas i slutna system [15].  Fenomenet fick namnet självorganisering eller autopoiesis. 

![Brusselator](brusselator.png)

*En visualisering av ett tillstånd för “Brusselatorn”, en av Prigogines modeller av kemiska system. Formerna uppstår genom kemisk självorganisering.*

Självorganiserande system har vissa likheter med levande varelser – båda ökar sin ordning och minskar därmed sin entropi. Båda åstadkommer detta genom att samla in energi från en energikälla. Men självorganisering är, till skillnad från livet, en enkel fysikalisk process som låter sig fångas även i enkla modeller. Självorganisering kräver både förstärkande återkoppling, som får mönstret att spridas, och korrigerande återkoppling, som stabiliserar det. Ett exempel är underkylt vatten: en enda droppe vatten som landar på en underkyld vägbana kan vara tillräcklig för att skapa en förstärkande återkoppling mellan vattenmolekylerna som får dem att kristallisera och täcka hela vägen med is. Kristallens bindningar fungerar sedan som en korrigerande återkoppling och förhindrar isen från att smälta igen. 

Fysiker har visserligen ofta lyckats beskriva laboratoriets studieobjekt som isolerade, men bara under kortvariga experiment. Förr eller senare läcker värmestrålning eller gasmolekyler eller något annat in eller ut ur varje system, oavsett hur väl vi isolerar det. Men är universum i sin helhet ett slutet system? Med den utgångspunkten förutspår fysiken att universum kommer bli allt kallare och mer utspritt, vilket kallas för “värmedöden”. Solsystemet och livet är i så fall bara en tillfällighet, en krusning på en yta som snart blir stilla igen. Om vi istället tänker oss att universum i sin helhet skulle vara öppet och flödande, att jämvikt aldrig nås, så blir självorganiseringen fundamental. Från det perspektivet skulle det vara laboratoriets slutna system och jämvikt som var undantaget.

Många har argumenterat för att materia, precis som levande varelser, kan ge upphov till form: från Demokritos atomism till Herakleitos flödes- och processfilosofi. Prigogine själv, i samarbete med filosofen Isabelle Stengers, försökte skapa en filosofi på självorganiseringens grund [16]. Sedan dess har begreppet fått stort inflytande: många nymaterialistiska filosofier lyfter fram självorganiseringen som ett tecken på en inneboende vitalitet hos materien. Sällan diskuteras självorganiseringens koppling till flöde och öppenhet [17]. Det går att fördjupa kritiken även mot fysikens “snälla” typ av förutsägbara öppenhet. Just eftersom flödet av energi var konstant genom Prigogines kemiska föreningar blev det förutsägbart och kunde inkorporeras i en matematisk modell. Men utanför laboratoriet är öppna system också öppna mot en oförutsägbar omvärld [18]. Självorganiseringen formuleras matematiskt som väldefinierade komponenter som interagerar enligt bestämda regler, vilket utgör ytterligare en sorts antagande om slutenhet. Donna Haraway föreslår begreppet sym-poiesis (blivande-tillsammans) för att beskriva ett formskapande där varken färdiga delar eller färdiga regler kan antas [18a]. Sym-poiesis är en bättre beskrivning av komplex biologisk eller politisk organisation, och kanske även kvantmekanisk. Sådana system kan inte studeras utan att ingripa och “samarbeta” med dem, men det utesluter inte att en matematisk modell är användbar i detta samarbete.


## Självorganisering i konsten

Återkoppling, självorganisering och komplexa system hade viss inverkan på konsten under senare halvan av 1900-talet. Genom sinnlighet och lek utvecklades en förkroppsligad kunskap om självorganisering som är mycket värdefull och kan ge ett kompletterande perspektiv. 



Den generativa konsten använde datorn för att skapa verk. Konstnärerna skapade modeller som simulerades för att ge upphov till bilder, ljud eller koreografier. Modellerna sågs dels som verktyg, dels som “autonoma” samarbetspartners till konstnären. Visionen var alltid att denna autonomi skulle utvecklas till en oberoende kreativitet [18b]. De bilder som var möjliga att skapa genom självorganiserade modeller, maskinernas faktiska uttryck, var avskalade och geometriska och påminde om suprematismen. Datorkonsten krävde att denna estetik togs som utgångspunkt. Vera Molnar skapade bilder som tecknades av en penna vilken i sin tur styrdes av ett datorprogram. Hon arbetade med modeller och simulationer, med återkopplingen mellan konstnären, koden och det autonoma generativa systemet. I denna metod ville hon hitta en rationell konceptuell metod för att upptäcka de djupare underliggande principerna bakom tillfredsställande bilder [18c]. 

Systemkonsten, exempelvis Hans Haacke genom sitt verk Condensation Cube, intresserade sig ursprungligen för hur återkoppling inom öppna nätverk skapade rytmer och mönster. Men den sökte sig snart till en mer radikal öppenhet och tog även in det större institutionella och politiska sammanhanget runt systemet. Det är en tendens som levt vidare och är i min mening kanske det viktigaste perspektivet som konsten kan erbjuda på öppna system. Samtidigt finns metoder för “infångning” även av öppna system: konstnären Agnieszka Kurant har i sitt verk A.A.I. utnyttjat termiter för att bygga upp skulpturer av färgstarka sandkorn. Från ett perspektiv skapas verken självorganiserat av termiterna själva, men i själva verket är de brickor i ett spel, deras arbete exploateras av konstnären utan att de ens är medvetna om exploateringen eller arbetets mål och mening. Kurant är mycket medveten om hur liknande fenomen är verksamma inom själva konstvärlden [19]. Samtidigt kan jag uppleva att självorganiseringen blir friktionsfri och foglig, nästan nihilistisk. Utgångspunkterna, läckagen och deras tilltäppande, allt det som gör systemet möjligt, förblir osynligt.

## Sociofysisk organisering: strävan efter jämvikt

Först studerade fysiker enbart slutna system som naturligt strävar mot jämvikt. Sedan började de studera dissipativa system genomkorsade av flöden, i de fall då flödet var förutsägbart. I de dissipativa modellerna återupptäcktes jämvikten i form av attraktorer, som en konsekvens av de förutsägbara flödena. Punktformiga attraktorer är krafter som skapar ett konstant tillstånd, medan cykliska attraktorer skapar repeterande flöden. Men det finns även märkliga eller kaotiska attraktorer, som driver dissipativa system mot oförutsägbarhet.

Inom det tidiga 1900-talets ekologi fanns föreställningen om att naturen bildade ekosystem som strävade mot ekologisk jämvikt, något som tidigt mötte kritik. Trots det blev föreställningen mycket spridd bland allmänheten. Miljörörelsen tog särskilt fasta på ekologisk balans, i vilken ekosystemet självorganiserar en harmonisk helhet medan endast människan ges agensen att störa systemet. När förespråkarna försökte hitta stöd för teorin i dissipativa modeller av populationer av rovdjur och bytesdjur, visade det sig att dessa är fyllda av kaotiska attraktorer. Oförutsägbarhet är snarare normen i ekologi [20]. Men föreställningen om ekologisk jämvikt har antagligen haft en mycket bred påverkan på vår föreställning även om mellanmänsklig organisering. Jämvikten passar allt för väl ihop med gamla bekanta föreställningar om harmoni, balans och det naturliga.

Sociofysikern Dirk Helbing argumenterar för en moderniserad version av Adam Smiths osynliga hand [21]. Han argumenterar för att agenter är kapabla att självorganisera både marknad och samhälle genom lokala interaktioner i form av lokal demokrati och fri företagsamhet. Mot det lokala ställer han ineffektiv påverkan “utifrån” i form av skatter och lagar, vilka leder till “turbulens” som stör systemets jämvikt. Helbing är kritisk till att marknader som de fungerar idag kan skapa ett rättvist samhälle, men han vänder sig bara mot den specifika "beräkning" som marknaden i praktiken utför när den sätter värde på olika tillgångar. Istället föreslår han nya rörelseekvationer för kapitalismen: han genomför simulationer av modeller där pengar ersätts med en “multi-dimensionell valuta”. Denna generaliserade valuta har enligt Helbing förmågan att beräkna ett annat samhälle, skapa ett självorganiserat beteende där rikedom inte nödvändigtvis koncentreras hos ett fåtal. Men vad gör vi om denna nya valuta leder till nya orättvisor? Helbing tar tydligt ställning för jämvikten som en utgångspunkt för samhällsorganisationen.

Trots kunskapen om såväl kaotiska attraktorer som begränsningen i dissipativa modeller fortsätter tron på balans, såväl som separationen i insida och utsida, att återkomma. På ett plan är det fråga om en scientistisk tro där simulation, självorganisering och attraktorer får ersätta religiösa förklaringar. På ett annat plan är vi omedvetet styrda av sociofysikens världsbild, den sätter gränserna för vad vi tror är möjligt. Den tredje delen av denna essä handlar om hur denna styrning, som i slutändan är en form av politik, fungerar.

# 3: Sociofysikens politik

## Den sociala atomen

Alla modeller i sociofysik utgår från att det finns en minsta byggsten: den sociala atomen. Gränsen dras ofta vid individen, som antas vara en avgränsad enhet: en agent som följer givna regler. Låt oss ta exemplet med folkmassan, som i vår modell beskrivs av en uppsättning separata agenter. Det innebär att vi gjort ett antagande om en “separation av skalor”. I det första steget kategoriseras processer som verkande på korta eller långa skalor: processer på korta skalor utgörs av kroppslig eller mental dynamik inom varje individ. Processer på långa skalor utgörs av individernas påverkan på varandra i massan. I det andra steget antar vi att de långa skalorna kan förstås oberoende av de korta, och vice versa. Konkret innebär det följande: vi formulerar regler för hur individerna påverkar varandra, vilka behandlas som statiska entiteter utan inre dynamik. Detta är uppenbarligen inte sant i en strikt mening. Våra inre processer spelar roll även för hur vi beter oss i en folkmassa. Separation av skalor är en mer eller mindre dålig approximation som i bästa fall kan ge ungefärliga resultat under en begränsad tidsrymd [22]. Ändå utgår så gott som alla modeller från att individen kan behandlas som en social atom. Utan sådana förenklingar blir nämligen modeller omöjliga att simulera och tappar sin förklarande kraft [23].

Den sociala atomen är ett så grundläggande antagande att vi kan behöva ta stöd från konsten för att ens bli medvetna om det. Konstnären Anna Ådahl har arbetat länge med massans politik och estetik. I sin utställning Default Characters, som visades på Marabouparkens konsthall 2018, undersökte hon agentbaserade simulationer av folkmassor, vilka används i filmindustrin för att skapa animerade stridsscener. I verken används bland annat video, skulptur och performance som överför agenternas växelverkan till koreografi. Massan har alltid varit ambivalent: beräknelig och oberäknelig, ett löfte och ett hot. Simulationens försök att begränsa den till en uppsättning agenter i en beräkningsbar interaktion blir i Anna Ådahls verk snarare en förskjutning som bevarar ambivalensen: stridsscenerna blir trovärdiga just på grund av sin oberäkningsbarhet inom den beräkningsbara simulationen. Upplevelsen av våldsamt hotfull agens – kvasi-agens – kvarstår. Samtidigt lyfter utställningen vikten av en standardinställning (default): agenternas beteende och utseende kan ställas in av filmskaparen, men förses med en standardinställning i filmskaparens datorprogram. Denna standard ger det förutsägbart oförutsägbara beteende som filmskaparen förväntas förvänta sig av agenterna. Precis som i valet av hyperparametrar kommer vi inte undan den subjektiva grunden. Ovissheten återkommer inuti den skenbara vissheten. Utställningen och verken behöver förstås från kompletterande perspektiv på massan, framför allt det performativa [24]. 

## Dressering

Som Pablo Jensen påpekat [23] så har fysiken historiskt sett inte bara anpassat sina modeller till systemet, utan även anpassat studieobjektet till modellerna. Atomerna studeras inte i det vilda, utan dresseras i laboratoriet för att kunna delta i den experimentella cirkusen [25]. Jensen påpekar att denna analogi blir bokstavlig i sociofysiken: det krävs inte bara att agenterna anpassas för att likna människor, utan att människorna anpassas för att bete sig som agenter. Ett exempel är hur vi tar hänsyn till att våra svar bedöms efter statistik, ofta av en maskin, när vi svarar på frågeformulär. Ett annat exempel är det direkta försöket till social dressering som utgörs av Kinas försök med “sociala krediter”.

Jensen lyfter fram hur sociofysiken bygger på att skala bort agenternas attribut för att nå fram till en förenklad modell. Utan förenklingar så är modellen inte mer än en samling fakta och tappar sin förklarande kraft. Men det finns i många fall flera förenklingar som kan göras, vilket leder till flera konkurrerande modeller av samma sociala system. Varje förenkling bär på sin mening, sin egen idé om vad som är viktigt att beakta. Det blir inte längre självklart att sociofysiken kan erbjuda några entydiga förklaringar.

## Kvasi-objektivitet

Donna Haraway har i flera texter påpekat hur vetenskapen genom skapandet av ett idealiserat studieobjekt också skapar sin egen uppsättning regler. Hon påpekar hur dessa regler framställs som objektiva och oberoende [26]. Här finns en parallell mellan vetenskapsfilosofi och marxistisk värdeteori. 

I Kapitalet lyfter Marx fram hur bytesvärde (till skillnad från bruksvärde) är en abstraktion som skapas av samhället [27]. Alla varor framstår som fyllda av en större eller mindre mängd av denna substans, som tar sin konkreta numeriska form i pengarna. Det är möjligt att ifrågasätta denna siffra och dess logik, men vi tvingas i praktiken att acceptera den i någon form för att överleva i ett kapitalistiskt samhälle. Marx kallar därför bytesvärdet för en “realabstraktion”. I Kapitalet betonas hur arbetet på ett plan är en vara bland andra, men på ett annat plan skapar värdet självt: just genom arbetet förvandlas vårt kvalitativa behov att överleva till ett kvantitativt värdeskapande. Marx lyfter fram hur den sociala ordningen för människa, furste och Gud under feodalismen framstår som precis lika objektiv och ofrånkomlig, sett inifrån det samhället. Moishe Postone har försökt generalisera marxistisk teori och särskilt realabstraktion [28]. Ett exempel är tid, som genom mätande som praktik förvandlas från en uppsättning händelser till en numerisk “tallinje”: abstrakt tid. För Postone är denna tallinje en social och politisk såväl som matematisk apparat, en “kvasi-objektiv form” liksom bytesvärdet. 

I sin bok Revolutionary Mathematics - Artificial Intelligence, Statistics and the Logic of Capitalism försöker Justin Joque förstå den allt mer dominerande statistiken och maskininlärningen som kvasi-objektiva former [6]. Till sin utgångspunkt tar han just marxistisk teori och särskilt Postone. Om arbetet i kapitalismen förvandlar kvalitativa behov till kvantitativt värde så föreställer sig Joque på motsvarande vis att statistiken förvandlar och förädlar konkreta och kvalitativt skilda fenomen och förlopp till kvantitativt ekvivalenta datapunkter. Ett exempel är de statistiska test som används för att undersöka effekten av psykologiska behandlingar i vården. Dessa test översätter kvalitativt mycket olika terapiformer till effektstyrkor som jämförs på en numerisk skala. När sådan statistik ens kritiseras så är det utifrån dess antaganden. En sådan kritik antar att det kvalitativt olika faktiskt skulle kunna jämföras om bara rätt antaganden kunde göras. Det Joque gör är istället att hitta en systemisk anledning till jämförandet, nämligen skapandet av kvasi-objektiva former. För Joque faller alla dessa antaganden tillbaka på referensklassproblemet inom statistik, som handlar om att statistiska resultat beror på hur vi indelar händelser i referensklasser [29]. Genom valet av referensklasser skapar statistikern den nödvändiga grunden för att kunna likställa det som är kvalitativt olika. 

Joque understryker att statistikens kvasi-objektiviteten inte går att tänka bort, lika lite som det går att tänka bort pengars värde eller Guds makt över den religiösa. Istället argumenterar han för att vår enda möjlighet är att försöka skapa nya och bättre kvasi-objektiva former.

## Social teknologi

Dirk Helbing lyfter fram begreppet “social teknologi”, i hans definition tekniska apparater som influerar agenternas lokala interaktioner och därigenom påverkar ett helt kollektiv. Han tänker sig mobiltelefoner och appar som tidiga varianter av “digitala assistenter” som fungerar genom att blanda simulation och verklighet, skapa en augmented reality för agenten som skiftar sitt beteende för att anpassa sig till den nya verkligheten. Helbing menar att metoden kan användas för att bland annat förhindra rusningar i folkmassor, förhindra trafikstockningar, sprida trovärdig information och skapa en multi-dimensionell valuta. 

Vi lever redan i en värld av social teknologi. Mobiltelefoner samlar in stora mängder data om oss och sociala medier strukturerar våra liv och modifierar vårt beteende. Ukrainska soldater använder appar för att decentraliserat koordinera trupprörelser och rikta artilleri mot måltavlor. Arbetsgivare kontrollerar – och motiverar – sina anställda genom appar, till exempel genom att övervaka och poängsätta de anställdas rörelser [30]. Gemensamt i alla dessa exempel är att den sociala teknologin uppmuntrar oss att bete oss som agenter i en “virtuell verklighet”, vilket ofta har kallats spelifiering (gamification). Vad som motiverar oss att faktiskt “spela spelet” kan vara både negativa och positiva stimuli. Det är tänkbart att det också finns fler drifter som får oss att vilja delta och frivilligt agera som en agent. Men den sociala teknologin tillåter också den eller de som kontrollerar den att modifiera rörelseekvationerna.

Kan social teknologi också användas för progressiv politisk organisering, kanske till och med för att skapa nya och bättre kvasi-objektiva former? Det finns flera initiativ till decentraliserad social media, men de verkar fokusera på säker och privat kommunikation snarare än spelifiering [31]. Decentraliserad koordination av stora demonstrationer fungerar däremot genom att modifiera deltagarnas beteende med en digital assistent. Zetkin är ett intressant projekt som startades av lokalföreningar av Vänsterpartiet i Sverige. Genom digitala verktyg kan verksamhet planeras och kartläggas och information kan lätt passera mellan medlemmar och organisatörer. Zetkin fungerar i viss mån som en digital assistent som påverkar användarnas beteende, får dem att se sin plats i ett större sammanhang och förvandlar organiseringen till en virtuell upplevelse. Men är den sociala teknologin kapabel att förvandla oss till mer än agenter plus rörelseekvationer? Finns det möjlighet att utforska sym-poiesis med hjälp av social teknologi? Den tekniska begränsningen riskerar att begränsa vår politik till ett autopoietiskt förhållningssätt.

## Omätbarhet och oklarhet

En kompletterande strategi är att söka sig aktivt till omätbarhet och oberäkningsbarhet. Mätandet är modellens spegel: mätningen fyller i en tabell där ett resultat i visst format redan väntas. Därför har även mätande en normativ makt. I Sverige har Jonna Bornemark visat hur offentlig såväl som privat verksamhet idag mäter och dokumenterar allt mer, till enorma mänskliga och ekonomiska kostnader. Vi sätter vår tillit till dessa system och mätvärden, snarare än att ha tillit till varandra. Om vi hade tillit till varandra skulle all denna “förpappring” kunna avskaffas, men vi behöver i så fall acceptera tillitens pris: fusk och bedrägeri.  Frågan är dock hur omätbarhet och politiskt motstånd hänger samman i detalj. I historien finns många exempel på repressiva statsapparater baserade på tillit. Dessutom riskerar omätbarheten att bli reaktionär istället för att skapa nya kvasi-objektiviteter. Den autonoma marxisten Franco “Bifo” Berardi vänder sig på ett mer politiskt riktat sätt mot ekonomins matematiska språk och hur det infiltrerat hela samhället. Han föreslår poetiska och konstnärliga former av motstånd [32].

Det anarkistiska kollektivet Tiqqun tänker sig omätbarhet och oberäkningsbarhet som en strategi och taktik [33]. De argumenterar för behovet av oklarhet (opacitet) i organisering: den som inte vill underkasta sig de härskandes kontroll måste skapa ogenomskinliga eller oklara zoner, platser som är uppbyggda så att deras arkitektur och invånare inte låter sig mätas eller beräknas. Omätbarheten blir här en strategi snarare än ett ideal i sig. Det finns flera exempel på hur oklarhet blivit en politisk strategi. Det är en välkänd faktor i krigföring i stadsmiljö, exempelvis i Gaza. ZAD de Notre-Dame-des-Landes är ett labyrintiskt ockuperat område i Frankrike som sedan 2007 hållit stånd mot polisens försök att nedmontera det. De svärmliknande demonstrationerna under klimattoppmöten har använt sig av brist på information för att förhindra att polisen förutser folkmassans rörelser. Men oklarheten har kanske ännu inte potentialen att bilda en ny kvasi-objektivitet.

En annan sorts omätbar strategi är att inspireras av biologin och ekologin. Donna Haraway i synnerhet har lyft fram sym-poiesis som modell för en politik av “sammanflätning” (entanglement) där vi slutar vara komponenter för varandra och börjar betrakta varandra som medskapare i blivande [18a]. Det är en strategi som återfinns i feministiska, dekoloniala och/eller ekologiska grupper, med många förgreningar och varianter som inte finns plats att utveckla i denna artikel. Mest lovande är kanske den medvetna ansatsen att skapa ett annat sätt att tänka, en annan logik av sammanflätning. Den biologiska världen blir också en konkret metod att skapa en oberäkningsbarhet som är specifik och konkret.

## Matematik bortom kapitalismen

Ännu en tänkbar strategi är att försöka hitta en progressiv kvasi-objektivitet inom själva matematiken, vilket Joque själv tar upp i sin bok [6]. Efter hundra år av konflikter om hur statistik ska förstås [5] blir idag Bayesianismen den allt mer dominerande tolkningen. Den föreställer sig statistikern själv som en agent i en modell. I denna modell har ingen agent perfekt kunskap, men en viss “övertygelse” (belief), en egen förståelse av verkligheten utifrån den kunskap agenten besitter. Genom en regel, Bayes teorem, uppdaterar varje agent sin övertygelse med nya fakta. Om agenterna följer dessa regler, visar modellen att objektivitet uppstår gradvis, som en sorts jämvikt. Bayesianismen hävdar att denna modell av kunskap förklarar hur vi ska förstå statistik och varför vi ska följa dess regler. Men själva modellen har en rad brister. Dels är den “ursprungliga övertygelsen” ambivalent, dels är det filosofiskt oklart varför agenten ska tro på modellen eller Bayes teorem från första början. 

De flesta försök att förklara varför vi ska agera som Bayesianska agenter och därmed tro på statistikens lagar använder sig av ett ekonomiskt argument. I en eller annan form är utgångspunkten att individer vill minska sina risker och maximera sin vinst i upprepade ekonomiska transaktioner. Det går att visa att med dessa utgångspunkter är det bäst för individen att ha tillit till statistikens lagar [6]. Från denna utgångspunkt krävs inte längre tillit eller tro på något annat än ekonomin, som ger upphov till statistikens lagar och sedan i förlängningen resten av naturvetenskapen. 

Skulle statistiken och i slutändan hela vetenskapen kräva att vi utgår från marknadens logik? Om kapitalistisk nytta avgör vårt val av metoder riskerar våra slutsatser att bli därefter. Målet är trots allt att förmera värde, oavsett om det innebär att ljuga eller på andra vis gå emot det allmännas bästa. Om marknaden har en logik så är den inte enhetlig, utan fylld av lögner och inkoherens. Joque tar som exempel hur det vid flera tillfällen upptäckts att bilföretag (senast Volkswagen) installerat system som upptäcker när bilens utsläpp testas, så kallade “defeat devices”. Systemen sänker bilens utsläpp till godkända nivåer så att bilen klarar testet. Det är fullständigt logiskt och har sin egen rationalitet, som samtidigt fullständigt går emot den rationalitet som vill minska utsläppen. Ett annat exempel är att många företag idag förfalskar maskininlärning/AI genom att använda billig mänsklig arbetskraft. Den vara som dessa företag säljer är inte en beprövad och välfungerande algoritm utan upplevelsen av att använda en sådan algoritm, inklusive all den tilltro och kvasi-objektivitet som vi tillskriver algoritmer. 

Joque menar att denna kris pekar på behovet av en ny och progressiv grund för statistiken som i förlängningen skulle kunna utvecklas till en ny kvasi-objektivitet [6]. Det finns redan en diskussion om vetenskapens utgångspunkter och politik, dess “standardinställningar”. Denna diskussion har fördjupats mycket tack vare klimatrörelsen. Joque lyfter fram det allmänna intellektet, som är ett namn inom marxismen för den kunskap vi besitter som ett kollektiv. Det utvecklas på universiteten och i samhället, fritt från kraven att skapa vinst. I våra arbeten tvingas vi sedan anpassa denna kunskap till att skapa mervärde för våra arbetsgivare. På grund av sin vinstlogik är företagen inte själva kapabla att skapa denna kunskap, samtidigt som de är beroende av den. Joque föreslår att det allmänna intellektet skulle kunna bryta sig fri från detta förhållande genom att stödja sig på en ny och progressiv vetenskap.

# Sammanfattning

Den här essän har handlat om försöken att förstå samhället som ett fysikaliskt system och hur denna ansats blivit allt mer inflytelserik. Jag har argumenterat för att det beror mer på politik än om hur framgångsrika dessa försök varit. Genom att förenkla individer till agenter så skapas inte bara förutsättningarna för att beräkna, utan också för att få människor att bete sig beräkningsbart. 

Kanske har jag underdrivit sociofysikens faktiska framgångar och positiva potential. Det hade inte varit möjligt för fysiken att lyckas påverka sociologi i så stor utsträckning om inte modell-skapande och simulation var kraftfulla metoder. Kraften kommer visserligen ur de förenklingar som gör det möjligt att urskilja orsaker i komplicerade data, men slutsatserna har inte varit triviala. De har lett till verkliga framsteg i förståelsen av mänskliga samhällen, vilket inte minst teorierna om nätverk visar. Kvantitativa metoder kan hjälpa oss att övervinna teoretiska begränsningar, som den mellan hierarki och decentralisering. Det finns troligtvis mycket mer att lära från modeller som bygger på den sociala atomen. 

Samtidigt är det tydligt att sociofysiken är ett mycket begränsat perspektiv på mänskliga samhällen. Kompletterande perspektiv är livsviktiga, särskilt för oss som organiserar för ett bättre samhälle, annars riskerar vi att inte se våra begränsningar och möjligheter. Kritiken och motståndet mot sociofysik riskerar att bli ett ytligt motstånd mot enskilda specialfall. Ett exempel är motståndet mot automatisk övervakning, som kritiseras på ett moraliskt eller till och med konspiratoriskt plan utan att någon djupare debatt uppstår om politiken i kvantitativa mått och modeller. Att förstå sociofysik från andra perspektiv – ekonomisk-historiska, filosofiska och kanske performativa – är alltså viktigt för att skapa utrymme för en politisk organisering i ett samhälle som redan är byggt på modeller och simulation.

# Referenser 


[1]: Isabelle Stengers. Cosmopolitics I. University of Minnesota Press, 2010.

[2]: M. Moussaïd, D. Helbing och G. Theraulaz. How simple rules determine pedestrian behavior and crowd disasters. PNAS 108 (17), 2011.

[3]: K. Haase, M. Kasper, M. Koch, S. Müller och D. Helbing. A pilgrim scheduling approach to increase public safety during the Hajj. Operations Research, 2015.

[4]: Scott Aaronson. P ?= NP. Open problems in mathematics, 2016.

[5]: Edwin Jaynes. Probability theory: The logic of science. Cambridge University Press,  2003 samt Judea Pearl och Dana MacKenzie. The book of why: The new science of cause and effect. Basic Books, 2018.

[6]: Justin Joque. Revolutionary mathematics: Artificial intelligence, statistics and the logic of capitalism. Verso Books, 2022.

[7]: Vladan Joller och Matteo Pasquinelli. The Nooscope Manifested, hämtad från https://kim.hfg-karlsruhe.de/nooscope.ai/

[8]: Sean Everton. Disrupting dark networks. Cambridge University Press, 2012.

[9]: Pamela Oliver och Daniel Myers. Networks, Diffusion, and Cycles of Collective Action. Social movements and networks: Relational approaches to collective action. Oxford University Press, 2003.

[10]: Albert-László Barabási och Albert Réka. Emergence of scaling in random networks. Science 286.5439, 1999. 

[11]: Rodrigo Nunes. Neither vertical nor horizontal: A theory of political organization. Verso Books, 2021.

[12]: Dominique Chu, Roger Strand och Ragnar Fjelland. Theories of complexity. Complexity 8.3, 2003

[13]: Katarzyna Sznajd-Weron, Józef Sznajd och Tomasz Weron. A review on the Sznajd model—20 years after. Physica A: Statistical Mechanics and its Applications 565, 2021. 

[13b]: Nigel Goldenfeld. Lectures on phase transitions and the renormalization group. CRC Press, 2018.

[14]: Wendy Chun. Discriminating Data. MIT Press, 2021.

[15]: Gregoire Nicolis och Ilya Prigogine. Self-organization in nonequilibrium systems: From dissipative structures to order through fluctuations. Wiley, 1977.

[16]: Ilya Prigogine och Isabelle Stengers. Order out of chaos: Man's new dialogue with nature. Verso Books, 2018.

[17]: Jane Bennett. Vibrant matter: A political ecology of things. Duke University Press, 2010.

[18]: Robert Rosen. Essays on life itself. Columbia University Press, 2000.

[18a]: Haraway, Donna J. Staying with the trouble: Making kin in the Chthulucene. Duke University Press, 2016.

[18b]: Jasia Reichardt. Computer Art. Essä i katalogen till utställningen Cybernetic Serendipity, 1968. 

[18c]: Grant Taylor. When the machine made art: the troubled history of computer art. Bloomsbury, 2014.

[19]: Intervju hämtad från http://evenmagazine.com/agnieszka-kurant/

[20]: Daniel Simberloff. The “balance of nature”—evolution of a panchreston. PLoS Biology 12.10, 2014.

[21]: Dirk Helbing. The automation of society is next: How to survive the digital revolution. https://dx.doi.org/10.2139/ssrn.2694312, 2015.

[22]: https://medium.com/complex-systems-channel/1-1-separation-of-scales-why-complex-systems-need-a-new-mathematics-d1598d632a6d

[23]: Pablo Jensen. The politics of physicists' social models. Comptes Rendus Physique 20.4, 2019 (https://arxiv.org/pdf/1903.00964.pdf)

[24]: Judith Butler. Notes toward a performative theory of assembly. Harvard University Press, 2015.

[25]: Andrew Pickering. The mangle of practice: Time, agency, and science. University of Chicago Press, 2010.

[26]: Donna Haraway. Modest Witness: Feminist diffractions in science studies, kapitel i boken The Disunity of Science: Boundaries, Contexts and Power. Stanford University Press, 1996 samt Steven Shapin. The invisible technician. New Scientist, 77(6), 1989.

[27]: Karl Marx. Kapitalet, band I, Arkiv Förlag 2018. (https://www.marxists.org/svenska/marx/1867/23-d101.htm)

[28]: Moishe Postone. Time, Labour and Social Domination. Cambridge University Press, 1993.

[29]: Alan Hájek. The reference class problem is your problem too. Synthese 156, 2007.

[30]: Julia Lindblom. Amazon : bakom framgången. Verbal Förlag, 2021

[31]: För exempel på decentraliserad social media, se exempelvis www.riseup.net eller www.mastodon.social.

[32]: Jonna Bornemark. Omätbarhetens renässans, Volante 2018 samt Horisonten finns alltid kvar. Volante 2020 samt Franco Berardi. The Uprising: On Poetry and Finance. Semiotext(e) / Intervention Series, 2012.

[33]: Tiqqun. The Cybernetic Hypothesis (https://theanarchistlibrary.org/library/tiqqun-the-cybernetic-hypothesis)

[34]: Grigory Volovik. The universe in a helium droplet. Vol. 117. OUP Oxford, 2003. (https://archive.org/download/grigory-vilovik-the-universe-in-a-helium-droplet/Grigory%20Vilovik%20-%20The%20Universe%20in%20a%20Helium%20Droplet.pdf)

