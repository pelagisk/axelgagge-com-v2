---
title: Sequestering
date: 2022-05-07T09:03:20-08:00
---

*Sequestering*, 2022, textile print graphing the Kuramoto-Sivashinsky differential equation

{{< imgrow >}}
  {{< img src="overview-1.jpg" >}}
{{< /imgrow >}}

{{< imgrow >}}
  {{< img src="printer-1.jpg" >}}
  {{< img src="printer-2.jpg" >}}
{{< /imgrow >}}

{{< imgrow >}}
  {{< img src="equation.jpg" >}}
{{< /imgrow >}}

{{< imgrow >}}
  {{< img src="printer-3.jpg" >}}
  {{< img src="from-above.jpg" >}}
{{< /imgrow >}}

{{< imgrow >}}
  {{< img src="overview-2.jpg" >}}
{{< /imgrow >}}

Ongoing investigation of leakages of information through autonomous systems.

The Kuramoto-Sivashinsky differential equation describes the spread of fire. The world it describes is chaotic and has an "arrow of time", but otherwise nothing much happens. It is still the subject of intense research.

This graph displays the speed of a laminar flame front as it develops over time.

*"Just as the sun shines through a glass – as though divested of body and substance – so the stars penetrate one another in the body... For the sun and the moon and all planets, as well as all the stars and the whole chaos, are in man..."*