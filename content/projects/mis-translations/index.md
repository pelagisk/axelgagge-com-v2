---
title: Mis-Translations
date: 2021-05-07T09:03:20-08:00
---

*Mis-Translations*, 2022, digital images from 3D renderings of equations

My contribution for the publication *Across and Beyond - Body and Landscape in Translation* by Lina Aastrup, Paulina Granat and Isabelle Ribe

{{< imgrow >}}
  {{< img src="1.jpg" >}}
  {{< img src="3.jpg" >}}
{{< /imgrow >}}

{{< imgrow >}}
  {{< img src="2.jpg" >}}
{{< /imgrow >}}

{{< imgrow >}}
  {{< img src="frontpage.jpg" >}}
  {{< img src="inside.jpg" >}}
{{< /imgrow >}}

Mis-Translations is an attempt to see translation through the lens of mathematics. Is translation a kind of transformation? Is translation only about language, or can shape and body, too, be translated? Can translation itself be given a body? What is leaked and what is added, as the concept of translation itself gets translated into mathematics?
Much of mathematics itself is a mis-translation. For example, the important concept of a “fiber bundle” in mathematics captures the idea of a surface covered in hair, like a fur or a rug: a leaking translation from the world of textiles and biology. In the end, it is perhaps the mistranslations that carry the greatest power?

During the release party, I read a text and played a sonification of the Kuramoto-Sivashinsky equation. You can listen to it here:

{{< soundcloud 1209115453 >}}

[You can download a digital copy of the book here.](https://su.diva-portal.org/smash/record.jsf?pid=diva2%3A1739432&dswid=-277)

[You can find information about the book and the release party here.](https://indexfoundation.se/talks-and-events/bookshop-situation-series-studies-in-curating-art-vii-across-and-beyond-body-and-landscape-in-translation)
