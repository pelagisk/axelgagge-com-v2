---
title: BFA exhibition
date: 2023-05-07T09:03:20-08:00
---


{{< img src="singers-1.jpg" >}}
{{< imgrow >}}
  {{< img src="singers-2.jpg" >}}
  {{< img src="singers-6.jpg" >}}
{{< /imgrow >}}
{{< imgrow >}}
  {{< img src="singers-4.jpg" >}}
  {{< img src="singers-5.jpg" >}}
  {{< img src="singers-3.jpg" >}}
{{< /imgrow >}}
{{< vimeo 865464188 >}}
*Sångare / Singers*, 2023, device to recreate singing sand with ø: 400-700 um

{{< img src="form-chains-salta.jpg" >}}
{{< imgrow >}}
  {{< img src="form-chains-1.jpg" >}}
  {{< img src="form-chains-2.jpg" >}}
{{< /imgrow >}}
*Bilda kedjor / Form Chains*, 2023, polariscope with analog simulation of the force between sand grains in a dune

{{< imgrow >}}
  {{< img src="salta-1.jpg" >}}
  {{< img src="salta-2.jpg" >}}
  {{< img src="salta-3.jpg" >}}
{{< /imgrow >}}
*Salta*, 2023, computer simulation of sand dune formation, running on Raspberry Pi 4

{{< imgrow >}}
  {{< img src="overview-1.jpg" >}}
  {{< img src="overview-2.jpg" >}}
{{< /imgrow >}}

[More documentation of the exhibition can be found here.](https://kkh.se/en/public-program/exhibitions/bachelors-show-in-pictures/)

[Information about the exhibition can be found here.](https://kkh.se/en/publikt-program/utstallningar/kandidatutstallning-bachelor-of-fine-arts/) 

Photos taken by Jean-Baptiste Beranger.